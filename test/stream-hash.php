<?php

use GuzzleHttp\Stream\Stream,
    GuzzleHttp\Stream\NullStream;

use function Krak\CacheBuster\staticStreamHash,
    Krak\CacheBuster\streamHash,
    Krak\CacheBuster\maxStreamHash;

describe('StreamHash', function() {
    describe('#staticStreamHash', function() {
        it('returns the input passed in', function() {
            $hash = staticStreamHash('abc');
            assert($hash(new NullStream()) == 'abc');
        });
    });
    describe('#streamHash', function() {
        it('hashes the contents of a stream', function() {
            $hash = streamHash('md5');
            assert(md5('') == $hash(new NullStream()));
        });
    });
    describe('#maxStreamHash', function() {
        it('returns max length hash of the stream', function() {
            $hash = maxStreamHash(staticStreamHash('abcd'), 3);
            assert($hash(new NullStream()) == 'abc');
        });
    });
});
