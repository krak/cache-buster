<?php

use Krak\CacheBuster\Asset\CacheBusterVersionStrategy;

describe('Asset', function() {
    describe('CacheBusterVersionStrategy', function() {
        beforeEach(function() {
            $this->vs = new CacheBusterVersionStrategy([
                'key' => [
                    'version' => '1',
                    'path' => '/key-1',
                    'filepath' => 'assets/key-1',
                ]
            ]);
        });
        describe('->getVersion($path)', function() {
            it('returns empty if the path is not found', function() {
                assert($this->vs->getVersion('b') === '');
            });
            it('returns the version if the path is found', function() {
                assert($this->vs->getVersion('key') === '1');
            });
            it('delegates to inner version strategy', function() {
                $vs = new CacheBusterVersionStrategy([], new MockVersionStrategy());
                assert($vs->getVersion('a') === 'abc');
            });
        });
        describe('->applyVersion($path)', function() {
            it('returns the path if no match was found', function() {
                assert($this->vs->applyVersion('abc') === 'abc');
            });
            it('returns the versioned path if match', function() {
                assert($this->vs->applyVersion('key') === '/key-1');
            });
            it('delegates to inner version strategy', function() {
                $vs = new CacheBusterVersionStrategy([], new MockVersionStrategy());
                assert($vs->applyVersion('a') === 'def');
            });
        });
    });
});

class MockVersionStrategy implements Symfony\Component\Asset\VersionStrategy\VersionStrategyInterface
{
    public function getVersion($path) {
        return 'abc';
    }

    public function applyVersion($path) {
        return 'def';
    }
}
