<?php

namespace Krak\CacheBuster;

class PathInfo
{
    private $dirname;
    private $basename;
    private $extension;
    private $filename;
    private $sep;

    public function __construct($parts, $sep = '/') {
        $this->dirname = $parts['dirname'];
        $this->basename = $parts['basename'];
        $this->extension = $parts['extension'];
        $this->filename = $parts['filename'];
        $this->sep = $sep;
    }

    public function getDirname() {
        return $this->dirname;
    }
    public function getBasename() {
        return $this->basename;
    }
    public function getExtension() {
        return $this->extension;
    }
    public function getFilename() {
        return $this->filename;
    }
    public function __toString() {
        return implode($this->sep, [$this->dirname, $this->basename]);
    }

    public static function createFromPath($path) {
        return new self(pathinfo($path));
    }

    public function withFilename($name) {
        $info = clone $this;
        $info->filename = $name;
        $info->basename = $info->filename . '.' . $info->extension;

        return $info;
    }

    public function withDirname($name) {
        $info = clone $this;
        $info->dirname = $dirname;

        return $info;
    }
}
