<?php

namespace Krak\CacheBuster\Command;

use Krak\CacheBuster,
    GuzzleHttp\Stream\Stream;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class BustCacheCommand extends Command
{
    protected function configure() {
        $types = [
            CacheBuster\TYPE_TIME,
            CacheBuster\TYPE_DATE,
            CacheBuster\TYPE_HASH
        ];
        $quote = function($val) {
            return '"' . $val . '"';
        };
        $types = implode(', ', array_map($quote, $types));

        $this->setName('krak:bust-cache')
            ->setDescription('Bust cache for compiled asset files')
            ->addArgument(
                'path',
                InputArgument::REQUIRED,
                'The path of the file to write'
            )
            ->addOption(
                'buster',
                'b',
                InputOption::VALUE_REQUIRED,
                'The cache buster type to use. Valid types are: ' . $types,
                CacheBuster\TYPE_HASH
            )
            ->addOption(
                'config-path',
                'c',
                InputOption::VALUE_REQUIRED,
                'The path to the config file to write',
                './asset-config.php'
            )
            ->addOption(
                'doc-root',
                'd',
                InputOption::VALUE_REQUIRED,
                'The document root of the site. Providing this value will make sure that the path stored will be relative to the doc root',
                './'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $path = $input->getArgument('path');
        $config_path = $input->getOption('config-path');
        $doc_root = $input->getOption('doc-root');
        $buster_type = $input->getOption('buster');

        $stdin = CacheBuster\wrapStdin(
            $buster_type,
            Stream::factory(fopen('php://stdin', 'r'))
        );
        $factory = CacheBuster\cacheBusterFactory($stdin);
        $config = new CacheBuster\BustCacheConfig(
            $factory($buster_type),
            new CacheBuster\ConfigIO\PHPConfigIO($config_path),
            $doc_root
        );

        CacheBuster\bustCache(
            $stdin,
            CacheBuster\PathInfo::createFromPath($path),
            $config
        );

        $stdin->close();
    }
}
