<?php

namespace Krak\CacheBuster;

class BustCacheConfig
{
    public $buster;
    public $config_io;
    public $doc_root;

    public function __construct($buster, ConfigIO\ConfigIO $config_io, $doc_root) {
        $this->buster = $buster;
        $this->config_io = $config_io;
        $this->doc_root = $doc_root;
    }
}
