<?php

namespace Krak\CacheBuster;

use GuzzleHttp\Stream\StreamInterface,
    GuzzleHttp\Stream\Utils;

function streamHash($algo = 'md5') {
    return function(StreamInterface $stream) use ($algo) {
        return Utils::hash($stream, $algo);
    };
}

/** limit the hash to a certain number of chars */
function maxStreamHash($hash, $max_chars) {
    return function(StreamInterface $stream) use ($hash, $max_chars) {
        return substr($hash($stream), 0, $max_chars);
    };
}

/** return the a static hash value */
function staticStreamHash($value) {
    return function(StreamInterface $stream) use ($value) {
        return $value;
    };
}
