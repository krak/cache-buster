<?php

namespace Krak\CacheBuster;

use GuzzleHttp\Stream\Stream,
    GuzzleHttp\Stream\CachingStream,
    GuzzleHttp\Stream\StreamInterface,
    GuzzleHttp\Stream\Utils;

const TYPE_TIME = 'time';
const TYPE_DATE = 'date';
const TYPE_HASH = 'hash';

function bustedFile($version, PathInfo $info) {
    return ['version' => $version, 'filepath' => (string) $info];
}

function wrapStdin($type, StreamInterface $stdin) {
    if ($type != TYPE_HASH) {
        return $stdin;
    }

    return new CachingStream($stdin);
}

function cacheBusterFactory(StreamInterface $stdin) {
    return function($type) use ($stdin) {
        if ($type == TYPE_TIME) {
            return timeCacheBuster($time);
        }
        else if ($type == TYPE_DATE) {
            return timeCacheBuster(function() {
                return date('Y-m-d');
            });
        }
        else if ($type == TYPE_HASH) {
            return hashCacheBuster($stdin);
        }

        throw new \Exception('Invalid bustery type: ' . $type);
    };
}

function timeCacheBuster($time = 'time') {
    return function(PathInfo $info) use ($time) {
        $version = $time();
        return bustedFile(
            $version,
            $info->withFilename($info->getFilename() . '-' . $version)
        );
    };
}

function suffixCacheBuster($suffix) {
    return function(PathInfo $info) use ($suffix) {
        return bustedFile(
            $suffix,
            $info->withFilename($info->getFilename() . $suffix)
        );
    };
}

function hashCacheBuster(StreamInterface $input, $stream_hash = null) {
    $stream_hash = $stream_hash ?: streamHash();
    return function(PathInfo $info) use ($input, $stream_hash) {
        $hash = $stream_hash($input);
        return bustedFile(
            $hash,
            $info->withFilename($info->getFilename() . '-' . $hash)
        );
    };
}

function _removeOldBustedFile($busted_file) {
    unlink($busted_file['filepath']);
}

function _pathFromDocRoot($path, $doc_root) {
    return str_replace($doc_root, '', $path);
}

/** entry point for actually performing the cache busting */
function bustCache(StreamInterface $input, PathInfo $path, BustCacheConfig $config) {
    $config_io = $config->config_io;
    $buster = $config->buster;
    $doc_root = $config->doc_root;

    $busted = $buster($path);
    $busted_file_config = $config_io->readConfig();

    // the non-cache busted file
    $path_key = _pathFromDocRoot((string) $path, $doc_root);

    if (array_key_exists($path_key, $busted_file_config)) {
        if ($busted['version'] == $busted_file_config[$path_key]['version']) {
            return; /* nothing to do */
        }

        _removeOldBustedFile($busted_file_config[$path_key]);
    }

    $busted['path'] = _pathFromDocRoot($busted['filepath'], $doc_root);

    $busted_file_config[$path_key] = $busted;
    $config_io->writeConfig($busted_file_config);

    $output = new Stream(fopen($busted['filepath'], 'w'));
    Utils::copyToStream($input, $output);
    $output->close();
}
