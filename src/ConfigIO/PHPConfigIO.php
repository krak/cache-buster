<?php

namespace Krak\CacheBuster\ConfigIO;

class PHPConfigIO implements ConfigIO
{
    private $path;

    public function __construct($path) {
        $this->path = $path;
    }

    public function readConfig() {
        if (!file_exists($this->path)) {
            return [];
        }

        return __include($this->path);
    }

    public function writeConfig($conf) {
        $tpl = <<<TPL
<?php

// this file was automatically generated by Krak\CacheBuster
return %s;
TPL;
        $contents = sprintf($tpl, var_export($conf, true));

        file_put_contents($this->path, $contents);
    }
}

function __include($__path) {
    return include $__path;
}
