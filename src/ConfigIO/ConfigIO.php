<?php

namespace Krak\CacheBuster\ConfigIO;

interface ConfigIO {
    public function readConfig();
    public function writeConfig($config);
}
