<?php

namespace Krak\CacheBuster\Asset;

use Symfony\Component\Asset\VersionStrategy\VersionStrategyInterface,
    Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy;

/** CacheBusterVersionStrategy is a decorator that takes a version map array
    created by the cache-buster and then wraps another strategy instance.
    If a path is found in the version map, then it returns the cache busted version,
    else it delegates to the decorated instance.
*/
class CacheBusterVersionStrategy implements VersionStrategyInterface
{
    private $version_map;
    private $strategy;

    public function __construct($version_map = [], VersionStrategyInterface $strategy = null) {
        $this->version_map = $version_map;
        $this->strategy = $strategy ?: new EmptyVersionStrategy();
    }

    public function getVersion($path) {
        if (!array_key_exists($path, $this->version_map)) {
            return $this->strategy->getVersion($path);
        }

        return $this->version_map[$path]['version'];
    }

    public function applyVersion($path) {
        if (!array_key_exists($path, $this->version_map)) {
            return $this->strategy->applyVersion($path);
        }

        return $this->version_map[$path]['path'];
    }
}
