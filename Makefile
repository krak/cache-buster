.PHONY: test

PERIDOT = ./vendor/bin/peridot

test:
	$(PERIDOT) -g '*.php' test
